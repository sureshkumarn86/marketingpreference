package com.tg.customer.marketingpreference.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.tg.customer.marketingpreference.MarketingpreferenceApplication;
import com.tg.customer.marketingpreference.dto.MarketingPreferenceDto;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.Objects;

@ExtendWith(SpringExtension.class)
@SpringBootTest(classes = MarketingpreferenceApplication.class,webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class MarketingPreferenceControllerITest {

    @LocalServerPort
    private int localServerPort;

    @Autowired
    private TestRestTemplate restTemplate;

    @BeforeEach
    public void setup() {
    }

    @Test
    void save() throws JsonProcessingException {
        MarketingPreferenceDto request = MarketingPreferenceDto.getInstance()
                .setFullName("Hammed Khan")
                .setPhone("98657223")
                .setEmail("hammed@none.ae")
                .setPostalAddress("562, AD, UAE")
                .setByEmail(true)
                .setBySms(true)
                .setByPost(false);
        ResponseEntity<MarketingPreferenceDto> responseEntity = restTemplate.postForEntity(getServiceUrl("/rest/marketing-preference"),
                request,
                MarketingPreferenceDto.class);
        System.out.println(new ObjectMapper().writeValueAsString(responseEntity.getBody()));
        Assertions.assertEquals(HttpStatus.OK,responseEntity.getStatusCode());
        Assertions.assertNotNull(responseEntity.getBody());
        Assertions.assertEquals(request.getEmail(),responseEntity.getBody().getEmail());
    }

    @Test
    void save_Invalid() {
        MarketingPreferenceDto request = MarketingPreferenceDto.getInstance()
//                .setFullName("Hammed Khan")
                .setPhone("98657223")
                .setEmail("hammed@none.ae")
                .setPostalAddress("562, AD, UAE")
                .setByEmail(true)
                .setBySms(true)
                .setByPost(false);
        ResponseEntity<MarketingPreferenceDto> responseEntity = restTemplate.postForEntity(getServiceUrl("/rest/marketing-preference"),
                request,
                MarketingPreferenceDto.class);
        Assertions.assertEquals(HttpStatus.BAD_REQUEST,responseEntity.getStatusCode());
    }

    @Test
    void save_Invalid2() {
        MarketingPreferenceDto request = MarketingPreferenceDto.getInstance()
                .setId(12L)
                .setPhone("98657223")
                .setEmail("hammed@none.ae")
                .setPostalAddress("562, AD, UAE")
                .setByEmail(true)
                .setBySms(true)
                .setByPost(false);
        ResponseEntity<MarketingPreferenceDto> responseEntity = restTemplate.postForEntity(getServiceUrl("/rest/marketing-preference"),
                request,
                MarketingPreferenceDto.class);
        Assertions.assertEquals(HttpStatus.BAD_REQUEST,responseEntity.getStatusCode());
    }

    @Test
    void update() {
        MarketingPreferenceDto request = MarketingPreferenceDto.getInstance()
                .setFullName("Hammed Khan")
                .setPhone("98657223")
                .setEmail("hammed@none.ae")
                .setPostalAddress("562, AD, UAE")
                .setByEmail(true)
                .setBySms(true)
                .setByPost(false);
        ResponseEntity<MarketingPreferenceDto> responseEntity = restTemplate.postForEntity(getServiceUrl("/rest/marketing-preference"),
                request,
                MarketingPreferenceDto.class);

        ResponseEntity<MarketingPreferenceDto> putResponse = restTemplate.exchange(getServiceUrl("/rest/marketing-preference"),
                HttpMethod.PUT, new HttpEntity<>(Objects.requireNonNull(responseEntity.getBody()).setByPost(true)), MarketingPreferenceDto.class);
        Assertions.assertTrue(Objects.requireNonNull(putResponse.getBody()).isByPost());
    }

    @Test
    void update_Invalid() {
        MarketingPreferenceDto request = MarketingPreferenceDto.getInstance()
                .setId(-2L)
                .setFullName("Hammed Khan")
                .setPhone("98657223")
                .setEmail("hammed@none.ae")
                .setPostalAddress("562, AD, UAE")
                .setByEmail(true)
                .setBySms(true)
                .setByPost(false);

        ResponseEntity<MarketingPreferenceDto> putResponse = restTemplate.exchange(getServiceUrl("/rest/marketing-preference"),
                HttpMethod.PUT, new HttpEntity<>(request), MarketingPreferenceDto.class);
        Assertions.assertEquals(HttpStatus.BAD_REQUEST, putResponse.getStatusCode());

    }

    private String getServiceUrl(String path) {
        return "http://localhost:" + localServerPort + path;
    }

}