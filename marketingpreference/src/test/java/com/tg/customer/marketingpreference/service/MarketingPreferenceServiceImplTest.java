package com.tg.customer.marketingpreference.service;

import com.tg.customer.marketingpreference.dto.MarketingPreferenceDto;
import com.tg.customer.marketingpreference.entity.MarketingPreference;
import com.tg.customer.marketingpreference.exception.DataException;
import com.tg.customer.marketingpreference.mapper.MarketingPreferenceMapper;
import com.tg.customer.marketingpreference.repository.MarketingPreferenceRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.*;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.context.ApplicationEventPublisher;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(MockitoExtension.class)
class MarketingPreferenceServiceImplTest {

    @Mock
    MarketingPreferenceRepository repository;

    @Mock
    ApplicationEventPublisher eventPublisher;

    @Spy
    MarketingPreferenceMapper mapper = new MarketingPreferenceMapper();

    @InjectMocks
    MarketingPreferenceServiceImpl service;

    @Test
    void save() {
        Mockito.when(repository.save(ArgumentMatchers.any(MarketingPreference.class))).thenAnswer(invocation -> {
            MarketingPreference argument = invocation.getArgument(0);
            argument.setId(1L);
            return argument;
        });
        MarketingPreferenceDto saved = service.save(MarketingPreferenceDto.getInstance().setFullName("Test")
                .setPhone("09380948203")
                .setEmail("test@test.com")
                .setPostalAddress("AE")
                .setByEmail(true)
                .setByPost(true));
        assertEquals(1L, saved.getId());
    }

    @Test
    void update() {
        Mockito.when(repository.save(ArgumentMatchers.any(MarketingPreference.class))).thenAnswer(invocation -> {
            MarketingPreference argument = invocation.getArgument(0);
            argument.setId(1L);
            return argument;
        });
        Mockito.when(repository.findById(1L)).thenAnswer(invocation -> {
            return Optional.of(new MarketingPreference().setId(1L));
        });
        MarketingPreferenceDto saved = service.update(MarketingPreferenceDto.getInstance()
                .setId(1L)
                .setFullName("Test")
                .setPhone("09380948203")
                .setEmail("test@test.com")
                .setPostalAddress("AE")
                .setByEmail(true)
                .setByPost(true));
        assertEquals(1L, saved.getId());
    }

    @Test
    void update_wrongId() {
        Mockito.when(repository.findById(1L)).thenAnswer(invocation -> {
            return Optional.empty();
        });
        assertThrows(DataException.class, () -> {
            service.update(MarketingPreferenceDto.getInstance()
                    .setId(1L)
                    .setFullName("Test")
                    .setPhone("09380948203")
                    .setEmail("test@test.com")
                    .setPostalAddress("AE")
                    .setByEmail(true)
                    .setByPost(true));
        });
    }
}