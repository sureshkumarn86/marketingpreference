package com.tg.customer.marketingpreference.exception;

public class DataException extends RuntimeException {
    public DataException(String s) {
        super(s);
    }
}
