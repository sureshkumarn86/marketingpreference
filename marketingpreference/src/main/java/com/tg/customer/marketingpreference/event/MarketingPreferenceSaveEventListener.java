package com.tg.customer.marketingpreference.event;

import com.tg.customer.marketingpreference.dto.MarketingPreferenceDto;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationEvent;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.transaction.event.TransactionalEventListener;
import org.springframework.web.client.RestTemplate;

import javax.validation.Valid;

@Service
public class MarketingPreferenceSaveEventListener {

    @Value("${application.ro.endpoint.replication}")
    private String roReplicationServiceUrl;
    private final Logger logger = LoggerFactory.getLogger(MarketingPreferenceSaveEventListener.class);

    @TransactionalEventListener
    @Async
    public void event(MarketingPreferenceSaveEvent event) {
        try{
            new RestTemplate().exchange(roReplicationServiceUrl, HttpMethod.POST, new HttpEntity<>(event.getSource()), MarketingPreferenceDto.class);
            logger.info("replication success. {}", event.getSource());
        } catch (Throwable t) {
            logger.error("Replication failed.", t);
            //TODO: fallback implementation
        }
    }
}
