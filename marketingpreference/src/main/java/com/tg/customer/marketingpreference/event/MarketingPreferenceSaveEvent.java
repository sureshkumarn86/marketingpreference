package com.tg.customer.marketingpreference.event;

import com.tg.customer.marketingpreference.dto.MarketingPreferenceDto;
import org.springframework.context.ApplicationEvent;

public class MarketingPreferenceSaveEvent extends ApplicationEvent {
    public MarketingPreferenceSaveEvent(MarketingPreferenceDto preferenceDto) {
        super(preferenceDto);
    }
}
