package com.tg.customer.marketingpreference;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableAsync;

@SpringBootApplication
@EnableAsync
public class MarketingpreferenceApplication {

    public static void main(String[] args) {
        SpringApplication.run(MarketingpreferenceApplication.class, args);
    }

}
