package com.tg.customer.marketingpreference.mapper;

import com.tg.customer.marketingpreference.dto.MarketingPreferenceDto;
import com.tg.customer.marketingpreference.entity.MarketingPreference;
import org.springframework.stereotype.Component;

@Component
public class MarketingPreferenceMapper {

    public void map(MarketingPreferenceDto dto, MarketingPreference entity) {
        entity.setFullName(dto.getFullName())
                .setEmail(dto.getEmail())
                .setPostalAddress(dto.getPostalAddress())
                .setPhone(dto.getPhone())
                .setByEmail(dto.isByEmail())
                .setByPost(dto.isByPost())
                .setBySms(dto.isBySms());
    }

    public MarketingPreferenceDto toDto(MarketingPreference entity) {
        return MarketingPreferenceDto.getInstance()
                .setId(entity.getId())
                .setFullName(entity.getFullName())
                .setEmail(entity.getEmail())
                .setPostalAddress(entity.getPostalAddress())
                .setPhone(entity.getPhone())
                .setByEmail(entity.isByEmail())
                .setByPost(entity.isByPost())
                .setBySms(entity.isBySms());


    }
}
