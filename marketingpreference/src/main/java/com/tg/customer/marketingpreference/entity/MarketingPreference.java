package com.tg.customer.marketingpreference.entity;

import javax.persistence.*;

@Entity
public class MarketingPreference {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column
    private String fullName;

    @Column
    private String email;

    @Column
    private String phone;

    @Column
    private String postalAddress;

    @Column
    private boolean byPost;

    @Column
    private boolean byEmail;

    @Column
    private boolean bySms;

    public Long getId() {
        return id;
    }

    public MarketingPreference setId(Long id) {
        this.id = id;
        return this;
    }

    public String getFullName() {
        return fullName;
    }

    public MarketingPreference setFullName(String fullName) {
        this.fullName = fullName;
        return this;
    }

    public String getEmail() {
        return email;
    }

    public MarketingPreference setEmail(String email) {
        this.email = email;
        return this;
    }

    public String getPhone() {
        return phone;
    }

    public MarketingPreference setPhone(String phone) {
        this.phone = phone;
        return this;
    }

    public String getPostalAddress() {
        return postalAddress;
    }

    public MarketingPreference setPostalAddress(String postalAddress) {
        this.postalAddress = postalAddress;
        return this;
    }

    public boolean isByPost() {
        return byPost;
    }

    public MarketingPreference setByPost(boolean byPost) {
        this.byPost = byPost;
        return this;
    }

    public boolean isByEmail() {
        return byEmail;
    }

    public MarketingPreference setByEmail(boolean byEmail) {
        this.byEmail = byEmail;
        return this;
    }

    public boolean isBySms() {
        return bySms;
    }

    public MarketingPreference setBySms(boolean bySms) {
        this.bySms = bySms;
        return this;
    }
}
