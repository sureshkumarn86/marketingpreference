package com.tg.customer.marketingpreference.service;

import com.tg.customer.marketingpreference.dto.MarketingPreferenceDto;
import com.tg.customer.marketingpreference.entity.MarketingPreference;
import com.tg.customer.marketingpreference.event.MarketingPreferenceSaveEvent;
import com.tg.customer.marketingpreference.exception.DataException;
import com.tg.customer.marketingpreference.mapper.MarketingPreferenceMapper;
import com.tg.customer.marketingpreference.repository.MarketingPreferenceRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.Optional;

@Service
public class MarketingPreferenceServiceImpl implements MarketingPreferenceService {
    private final MarketingPreferenceRepository repository;
    private final MarketingPreferenceMapper mapper;
    private final ApplicationEventPublisher applicationEventPublisher;
    private final Logger logger = LoggerFactory.getLogger(MarketingPreferenceServiceImpl.class);

    public MarketingPreferenceServiceImpl(MarketingPreferenceRepository repository, MarketingPreferenceMapper mapper, ApplicationEventPublisher applicationEventPublisher) {
        this.repository = repository;
        this.mapper = mapper;
        this.applicationEventPublisher = applicationEventPublisher;
    }

    @Transactional
    public MarketingPreferenceDto save(MarketingPreferenceDto marketingPreferenceDto) {
        logger.info("saving for {}", marketingPreferenceDto.getFullName());
        MarketingPreference entity = new MarketingPreference();
        mapper.map(marketingPreferenceDto, entity);
        MarketingPreferenceDto preferenceDto = mapper.toDto(repository.save(entity));
        applicationEventPublisher.publishEvent(new MarketingPreferenceSaveEvent(preferenceDto));
        return preferenceDto;
    }

    @Transactional
    public MarketingPreferenceDto update(MarketingPreferenceDto marketingPreferenceDto) {
        logger.info("updating for {}", marketingPreferenceDto.getId());
        Optional<MarketingPreference> existing = repository.findById(marketingPreferenceDto.getId());
        if (!existing.isPresent()) {
            throw new DataException("Invalid Id");
        }
        MarketingPreference entity = existing.get();
        mapper.map(marketingPreferenceDto, entity);
        MarketingPreference saved = repository.save(entity);
        MarketingPreferenceDto preferenceDto = mapper.toDto(saved);
        applicationEventPublisher.publishEvent(new MarketingPreferenceSaveEvent(preferenceDto));
        return preferenceDto;
    }
}
