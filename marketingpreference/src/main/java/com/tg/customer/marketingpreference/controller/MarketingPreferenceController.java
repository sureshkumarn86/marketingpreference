package com.tg.customer.marketingpreference.controller;

import com.tg.customer.marketingpreference.dto.MarketingPreferenceDto;
import com.tg.customer.marketingpreference.exception.DataException;
import com.tg.customer.marketingpreference.service.MarketingPreferenceService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Objects;

@RestController
@RequestMapping("/rest")
public class MarketingPreferenceController {

    private final MarketingPreferenceService marketingPreferenceService;

    public MarketingPreferenceController(MarketingPreferenceService marketingPreferenceService) {
        this.marketingPreferenceService = marketingPreferenceService;
    }

    @PostMapping("/marketing-preference")
    public ResponseEntity<MarketingPreferenceDto> save(@RequestBody @Valid MarketingPreferenceDto marketingPreferenceDto) {
        return ResponseEntity.ok(marketingPreferenceService.save(marketingPreferenceDto));
    }

    @PutMapping("/marketing-preference")
    public ResponseEntity<MarketingPreferenceDto> update(@RequestBody @Valid MarketingPreferenceDto marketingPreferenceDto) {
        return ResponseEntity.ok(marketingPreferenceService.update(marketingPreferenceDto));
    }
}
