package com.tg.customer.marketingpreference.repository;

import com.tg.customer.marketingpreference.entity.MarketingPreference;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface MarketingPreferenceRepository extends JpaRepository<MarketingPreference, Long> {
}
