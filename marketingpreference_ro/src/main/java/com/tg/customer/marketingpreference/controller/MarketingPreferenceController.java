package com.tg.customer.marketingpreference.controller;

import com.tg.customer.marketingpreference.dto.MarketingPreferenceDto;
import com.tg.customer.marketingpreference.exception.DataException;
import com.tg.customer.marketingpreference.service.MarketingPreferenceService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Objects;

@RestController
@RequestMapping("/rest")
public class MarketingPreferenceController {

    private final MarketingPreferenceService marketingPreferenceService;

    public MarketingPreferenceController(MarketingPreferenceService marketingPreferenceService) {
        this.marketingPreferenceService = marketingPreferenceService;
    }

    @GetMapping("/marketing-preference/{id}")
    public ResponseEntity<MarketingPreferenceDto> get(@PathVariable Long id) {
        return ResponseEntity.of(marketingPreferenceService.get(id));
    }
    @PostMapping("/marketing-preference")
    public ResponseEntity<MarketingPreferenceDto> replicate(@RequestBody MarketingPreferenceDto dto) {
        return ResponseEntity.ok(marketingPreferenceService.replicate(dto));
    }
}
