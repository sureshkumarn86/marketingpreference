package com.tg.customer.marketingpreference.service;

import com.tg.customer.marketingpreference.dto.MarketingPreferenceDto;
import com.tg.customer.marketingpreference.entity.MarketingPreference;
import com.tg.customer.marketingpreference.exception.DataException;
import com.tg.customer.marketingpreference.mapper.MarketingPreferenceMapper;
import com.tg.customer.marketingpreference.repository.MarketingPreferenceRepository;
import org.springframework.stereotype.Service;

import java.util.Optional;

public interface MarketingPreferenceService {

     Optional<MarketingPreferenceDto> get(Long id) ;

     MarketingPreferenceDto replicate(MarketingPreferenceDto dto);
}
