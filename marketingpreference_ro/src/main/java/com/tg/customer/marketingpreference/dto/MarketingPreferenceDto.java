package com.tg.customer.marketingpreference.dto;

import javax.validation.constraints.NotBlank;

public class MarketingPreferenceDto {

    private Long id;

    @NotBlank(message = "Name is mandatory")
    private String fullName;
    
    private String email;
    
    private String phone;
    
    private String postalAddress;
    
    private boolean byPost;
    
    private boolean byEmail;
    
    private boolean bySms;

    public Long getId() {
        return id;
    }

    public MarketingPreferenceDto setId(Long id) {
        this.id = id;
        return this;
    }

    public String getFullName() {
        return fullName;
    }

    public MarketingPreferenceDto setFullName(String fullName) {
        this.fullName = fullName;
        return this;
    }

    public String getEmail() {
        return email;
    }

    public MarketingPreferenceDto setEmail(String email) {
        this.email = email;
        return this;
    }

    public String getPhone() {
        return phone;
    }

    public MarketingPreferenceDto setPhone(String phone) {
        this.phone = phone;
        return this;
    }

    public String getPostalAddress() {
        return postalAddress;
    }

    public MarketingPreferenceDto setPostalAddress(String postalAddress) {
        this.postalAddress = postalAddress;
        return this;
    }

    public boolean isByPost() {
        return byPost;
    }

    public MarketingPreferenceDto setByPost(boolean byPost) {
        this.byPost = byPost;
        return this;
    }

    public boolean isByEmail() {
        return byEmail;
    }

    public MarketingPreferenceDto setByEmail(boolean byEmail) {
        this.byEmail = byEmail;
        return this;
    }

    public boolean isBySms() {
        return bySms;
    }

    public MarketingPreferenceDto setBySms(boolean bySms) {
        this.bySms = bySms;
        return this;
    }

    public static MarketingPreferenceDto getInstance() {
        return new MarketingPreferenceDto();
    }
}
