package com.tg.customer.marketingpreference.service;

import com.tg.customer.marketingpreference.dto.MarketingPreferenceDto;
import com.tg.customer.marketingpreference.entity.MarketingPreference;
import com.tg.customer.marketingpreference.exception.DataException;
import com.tg.customer.marketingpreference.mapper.MarketingPreferenceMapper;
import com.tg.customer.marketingpreference.repository.MarketingPreferenceRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class MarketingPreferenceServiceImpl implements MarketingPreferenceService {
    private final MarketingPreferenceRepository repository;
    private final MarketingPreferenceMapper mapper;
    private final Logger logger = LoggerFactory.getLogger(MarketingPreferenceServiceImpl.class);

    public MarketingPreferenceServiceImpl(MarketingPreferenceRepository repository, MarketingPreferenceMapper mapper) {
        this.repository = repository;
        this.mapper = mapper;
    }

    public Optional<MarketingPreferenceDto> get(Long id) {
        return repository.findById(id).map(mapper::toDto);
    }

    @Override
    public MarketingPreferenceDto replicate(MarketingPreferenceDto dto) {
        logger.info("replicating for id :{}", dto.getId());
        Optional<MarketingPreference> existing = repository.findById(dto.getId());
        MarketingPreference entity = existing.orElse(new MarketingPreference().setId(dto.getId()));
        mapper.map(dto, entity);
        MarketingPreference saved = repository.save(entity);
        return mapper.toDto(saved);
    }
}
