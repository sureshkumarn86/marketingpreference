package com.tg.customer.marketingpreference.service;

import com.tg.customer.marketingpreference.dto.MarketingPreferenceDto;
import com.tg.customer.marketingpreference.entity.MarketingPreference;
import com.tg.customer.marketingpreference.mapper.MarketingPreferenceMapper;
import com.tg.customer.marketingpreference.repository.MarketingPreferenceRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(MockitoExtension.class)
class MarketingPreferenceServiceImplTest {

    @Mock
    MarketingPreferenceRepository repository;

    @Spy
    MarketingPreferenceMapper mapper;

    @InjectMocks
    MarketingPreferenceServiceImpl service;

    @Test
    void get() {
        Mockito.when(repository.findById(1L)).thenReturn(Optional.of(new MarketingPreference().setId(1L).setFullName("Test")));
        Optional<MarketingPreferenceDto> preferenceDto = service.get(1L);
        assertTrue(preferenceDto.isPresent());
    }

    @Test
    void get_False() {
        Mockito.when(repository.findById(1L)).thenReturn(Optional.empty());
        Optional<MarketingPreferenceDto> preferenceDto = service.get(1L);
        assertFalse(preferenceDto.isPresent());
    }
}