package com.tg.customer.marketingpreference.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.tg.customer.marketingpreference.MarketingpreferenceApplication;
import com.tg.customer.marketingpreference.dto.MarketingPreferenceDto;
import com.tg.customer.marketingpreference.entity.MarketingPreference;
import com.tg.customer.marketingpreference.repository.MarketingPreferenceRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.Objects;

@ExtendWith(SpringExtension.class)
@SpringBootTest(classes = MarketingpreferenceApplication.class,webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class MarketingPreferenceControllerITest {

    @LocalServerPort
    private int localServerPort;

    @Autowired
    private TestRestTemplate restTemplate;

    @Autowired
    private MarketingPreferenceRepository repository;

    @BeforeEach
    public void setup() {
    }

    @Test
    void get() throws JsonProcessingException {
        MarketingPreference request = repository.save( new MarketingPreference()
                .setFullName("Hammed Khan")
                .setPhone("98657223")
                .setEmail("hammed@none.ae")
                .setPostalAddress("562, AD, UAE")
                .setByEmail(true)
                .setBySms(true)
                .setByPost(false));
        ResponseEntity<MarketingPreferenceDto> responseEntity = restTemplate.getForEntity(getServiceUrl("/rest/marketing-preference/{id}"),
                MarketingPreferenceDto.class, request.getId());
        System.out.println(new ObjectMapper().writeValueAsString(responseEntity.getBody()));
        Assertions.assertEquals(HttpStatus.OK,responseEntity.getStatusCode());
        Assertions.assertNotNull(responseEntity.getBody());
        Assertions.assertEquals(request.getEmail(),responseEntity.getBody().getEmail());
    }

    @Test
    void get_404() throws JsonProcessingException {
        ResponseEntity<MarketingPreferenceDto> responseEntity = restTemplate.getForEntity(getServiceUrl("/rest/marketing-preference/{id}"),
                MarketingPreferenceDto.class, 404);
        System.out.println(new ObjectMapper().writeValueAsString(responseEntity.getBody()));
        Assertions.assertEquals(HttpStatus.NOT_FOUND,responseEntity.getStatusCode());
    }


    private String getServiceUrl(String path) {
        return "http://localhost:" + localServerPort + path;
    }

}