# Getting Started
### Run Tests
bash into the directory and run`./gradlew :test`
### Run The Application
Pre requisites:
    mysql running in localhost and has a db marketingpreference_ro
    with creds:root/password
1. bash into the project directory
2. run`./gradlew :bootRun`
2. open [api-doc](http://localhost:8081/swagger-ui.html) (or)
2. open any rest client and post data. eg. below.
    ```
   GET http://localhost:8080/rest/marketing-preference/1
      ```
 2. For read service refer the another project
### Containerization
`docker build -t marketingpreference_ro .`
run parent docker compose for collective run.
### Reference Documentation
For further reference, please consider the following sections:

* [Official Gradle documentation](https://docs.gradle.org)
* [Spring Boot Gradle Plugin Reference Guide](https://docs.spring.io/spring-boot/docs/2.5.4/gradle-plugin/reference/html/)
* [Create an OCI image](https://docs.spring.io/spring-boot/docs/2.5.4/gradle-plugin/reference/html/#build-image)
* [Spring Web](https://docs.spring.io/spring-boot/docs/2.5.4/reference/htmlsingle/#boot-features-developing-web-applications)
* [Spring Data JPA](https://docs.spring.io/spring-boot/docs/2.5.4/reference/htmlsingle/#boot-features-jpa-and-spring-data)

### Guides
The following guides illustrate how to use some features concretely:

* [Building a RESTful Web Service](https://spring.io/guides/gs/rest-service/)
* [Serving Web Content with Spring MVC](https://spring.io/guides/gs/serving-web-content/)
* [Building REST services with Spring](https://spring.io/guides/tutorials/bookmarks/)
* [Accessing Data with JPA](https://spring.io/guides/gs/accessing-data-jpa/)

### Additional Links
These additional references should also help you:

* [Gradle Build Scans – insights for your project's build](https://scans.gradle.com#gradle)

